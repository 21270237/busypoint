package busyPoint;


import java.awt.Toolkit;

public class Cost extends javax.swing.JFrame {
    
    public int id = ProjectManager.id;
    public ProjectManager pm = new ProjectManager();
    

    public Cost() {
        initComponents();
        setIcon();
        changeTexts();
    }

   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        CostPane = new javax.swing.JTabbedPane();
        miscPanel = new javax.swing.JPanel();
        miscTxt = new javax.swing.JLabel();
        miscCostVar = new javax.swing.JLabel();
        setMiscCost = new javax.swing.JButton();
        staffPanel = new javax.swing.JPanel();
        staffTxt = new javax.swing.JLabel();
        staffCostVar = new javax.swing.JLabel();
        setStaffCostButton = new javax.swing.JButton();
        finishedButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Promotion:"+id);
        setResizable(false);

        CostPane.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);

        miscPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        miscTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        miscTxt.setText("Current misc estimate:");

        miscCostVar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        miscCostVar.setForeground(new java.awt.Color(225, 0, 0));
        miscCostVar.setText("Not set");

        setMiscCost.setText("Set Cost");
        setMiscCost.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setMiscCost(evt);
            }
        });

        javax.swing.GroupLayout miscPanelLayout = new javax.swing.GroupLayout(miscPanel);
        miscPanel.setLayout(miscPanelLayout);
        miscPanelLayout.setHorizontalGroup(
            miscPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(miscPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(miscTxt)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(miscCostVar)
                .addContainerGap(45, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, miscPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(setMiscCost)
                .addContainerGap())
        );
        miscPanelLayout.setVerticalGroup(
            miscPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(miscPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(miscPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(miscTxt)
                    .addComponent(miscCostVar))
                .addGap(18, 18, 18)
                .addComponent(setMiscCost)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        CostPane.addTab("Misc", miscPanel);

        staffPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        staffTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        staffTxt.setText("Current staff estimate:");

        staffCostVar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        staffCostVar.setForeground(new java.awt.Color(225, 0, 0));
        staffCostVar.setText("Not set");

        setStaffCostButton.setText("Set Cost");
        setStaffCostButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setStaffCost(evt);
            }
        });

        javax.swing.GroupLayout staffPanelLayout = new javax.swing.GroupLayout(staffPanel);
        staffPanel.setLayout(staffPanelLayout);
        staffPanelLayout.setHorizontalGroup(
            staffPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(staffPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(staffTxt)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(staffCostVar)
                .addContainerGap(46, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, staffPanelLayout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(setStaffCostButton)
                .addContainerGap())
        );
        staffPanelLayout.setVerticalGroup(
            staffPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(staffPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(staffPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(staffTxt)
                    .addComponent(staffCostVar))
                .addGap(18, 18, 18)
                .addComponent(setStaffCostButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        CostPane.addTab("Staff", staffPanel);

        finishedButton.setText("Finished");
        finishedButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finishedButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(CostPane)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(finishedButton)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(CostPane, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(finishedButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void setStaffCost(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setStaffCost
        dispose();
        new Staff().setVisible(true);
    }//GEN-LAST:event_setStaffCost

    private void setMiscCost(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setMiscCost
        dispose();
        new Misc().setVisible(true);
    }//GEN-LAST:event_setMiscCost

    private void finishedButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finishedButtonActionPerformed
        dispose();
        new Promotion().setVisible(true);
    }//GEN-LAST:event_finishedButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTabbedPane CostPane;
    private javax.swing.JButton finishedButton;
    public javax.swing.JLabel miscCostVar;
    private javax.swing.JPanel miscPanel;
    private javax.swing.JLabel miscTxt;
    private javax.swing.JButton setMiscCost;
    private javax.swing.JButton setStaffCostButton;
    public javax.swing.JLabel staffCostVar;
    private javax.swing.JPanel staffPanel;
    private javax.swing.JLabel staffTxt;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.png")));
    }

   

    private void changeTexts() {
        pm.getConnection(id);
        staffCostVar.setText(Integer.toString(pm.staffCost));
        miscCostVar.setText(Integer.toString(pm.miscCost));  
        pm.closeConnection();
    }

}
