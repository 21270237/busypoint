package busyPoint;

import java.awt.Toolkit;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.concurrent.TimeUnit;
import javax.swing.JOptionPane;
import javax.swing.JPanel;

public class Promotion extends javax.swing.JFrame {

    public int id = ProjectManager.id;
    public ProjectManager pm = new ProjectManager();
    public SimpleDateFormat f = new SimpleDateFormat("dd/MM/yyyy");

    public Promotion() {
        initComponents();
        setIcon();
        changeTexts();

    }
   
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        finishButton = new javax.swing.JButton();
        tabbs = new javax.swing.JTabbedPane();
        costTabb = new javax.swing.JPanel();
        staffCostTxt = new javax.swing.JLabel();
        miscCostTxt = new javax.swing.JLabel();
        setCostButton = new javax.swing.JButton();
        separator = new javax.swing.JSeparator();
        totalCostTxt = new javax.swing.JLabel();
        staffCostVar = new javax.swing.JLabel();
        miscCostVar = new javax.swing.JLabel();
        totalCostVar = new javax.swing.JLabel();
        dateTabb = new javax.swing.JPanel();
        endDateTxt = new javax.swing.JLabel();
        setDateButton = new javax.swing.JButton();
        dateVar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Promotion: "+id);
        setResizable(false);

        finishButton.setText("Finish");
        finishButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finishButtonActionPerformed(evt);
            }
        });

        tabbs.setTabLayoutPolicy(javax.swing.JTabbedPane.SCROLL_TAB_LAYOUT);
        tabbs.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tabbs.setName(""); // NOI18N

        costTabb.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));
        costTabb.setToolTipText("");

        staffCostTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        staffCostTxt.setText("Staff cost is: ");

        miscCostTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        miscCostTxt.setText("Misc cost is: ");

        setCostButton.setText("Set Cost");
        setCostButton.setToolTipText("Set Date firste");
        setCostButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setCost(evt);
            }
        });

        totalCostTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        totalCostTxt.setText("The total cost is:");

        staffCostVar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        staffCostVar.setForeground(new java.awt.Color(255, 0, 0));
        staffCostVar.setText("Not set");

        miscCostVar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        miscCostVar.setForeground(new java.awt.Color(255, 0, 0));
        miscCostVar.setText("Not set");

        totalCostVar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        totalCostVar.setForeground(new java.awt.Color(255, 0, 0));
        totalCostVar.setText("Not set");

        javax.swing.GroupLayout costTabbLayout = new javax.swing.GroupLayout(costTabb);
        costTabb.setLayout(costTabbLayout);
        costTabbLayout.setHorizontalGroup(
            costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(costTabbLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(costTabbLayout.createSequentialGroup()
                        .addGroup(costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(costTabbLayout.createSequentialGroup()
                                .addComponent(miscCostTxt)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(miscCostVar)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(setCostButton))
                            .addGroup(costTabbLayout.createSequentialGroup()
                                .addGroup(costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addGroup(costTabbLayout.createSequentialGroup()
                                        .addComponent(staffCostTxt)
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                        .addComponent(staffCostVar))
                                    .addComponent(separator, javax.swing.GroupLayout.PREFERRED_SIZE, 244, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(costTabbLayout.createSequentialGroup()
                        .addComponent(totalCostTxt)
                        .addGap(18, 18, 18)
                        .addComponent(totalCostVar)
                        .addGap(0, 0, Short.MAX_VALUE))))
        );
        costTabbLayout.setVerticalGroup(
            costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(costTabbLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(staffCostTxt)
                    .addComponent(staffCostVar))
                .addGap(18, 18, 18)
                .addGroup(costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(miscCostTxt)
                    .addComponent(miscCostVar)
                    .addComponent(setCostButton))
                .addGap(13, 13, 13)
                .addComponent(separator, javax.swing.GroupLayout.PREFERRED_SIZE, 11, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(costTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(totalCostTxt)
                    .addComponent(totalCostVar))
                .addContainerGap(26, Short.MAX_VALUE))
        );

        tabbs.addTab("Cost", costTabb);

        dateTabb.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        endDateTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        endDateTxt.setText("Current end date is:");

        setDateButton.setText("Set Date");
        setDateButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setDate(evt);
            }
        });

        dateVar.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dateVar.setForeground(new java.awt.Color(255, 0, 0));
        dateVar.setText("Not set");

        javax.swing.GroupLayout dateTabbLayout = new javax.swing.GroupLayout(dateTabb);
        dateTabb.setLayout(dateTabbLayout);
        dateTabbLayout.setHorizontalGroup(
            dateTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dateTabbLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(dateTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dateTabbLayout.createSequentialGroup()
                        .addGap(10, 10, 10)
                        .addComponent(dateVar)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 115, Short.MAX_VALUE)
                        .addComponent(setDateButton))
                    .addGroup(dateTabbLayout.createSequentialGroup()
                        .addComponent(endDateTxt)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        dateTabbLayout.setVerticalGroup(
            dateTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(dateTabbLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(endDateTxt)
                .addGroup(dateTabbLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(dateTabbLayout.createSequentialGroup()
                        .addGap(12, 12, 12)
                        .addComponent(dateVar))
                    .addGroup(dateTabbLayout.createSequentialGroup()
                        .addGap(18, 18, 18)
                        .addComponent(setDateButton)))
                .addContainerGap(73, Short.MAX_VALUE))
        );

        tabbs.addTab("Date", dateTabb);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbs, javax.swing.GroupLayout.PREFERRED_SIZE, 275, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(finishButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(tabbs, javax.swing.GroupLayout.PREFERRED_SIZE, 179, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(finishButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        tabbs.getAccessibleContext().setAccessibleName("tab");

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void setDate(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setDate
        dispose();
        JOptionPane.showMessageDialog(null, " After you set the date,"
                + "\n you need to press set cost again");
        new DateClass().setVisible(true);
    }//GEN-LAST:event_setDate

    private void setCost(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setCost
        
        int reply = JOptionPane.showConfirmDialog(null, " Have you set the correct date ? ", "Check", JOptionPane.YES_NO_OPTION);
        if (reply == JOptionPane.YES_OPTION) {
            dispose();

            Date curr = new Date();
            long diff = pm.finalDate.getTime() - curr.getTime();
            int day = (int) TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

            if (day < 0) {
                JOptionPane.showMessageDialog(new JPanel(), " Please select a future date. \n Current final date is set in the past", "Error", JOptionPane.ERROR_MESSAGE);
                new Promotion().setVisible(true);
            } else if (day < 5) {
                JOptionPane.showMessageDialog(new JPanel(), " A time period of " + day
                        + " days is too short\n Please modify the date", "Error", JOptionPane.ERROR_MESSAGE);
                new Promotion().setVisible(true);
            } else {
                new Cost().setVisible(true);
            }
        }

    }//GEN-LAST:event_setCost

    private void finishButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finishButtonActionPerformed
        dispose();
        new ProjectManager().setVisible(true);

    }//GEN-LAST:event_finishButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel costTabb;
    private javax.swing.JPanel dateTabb;
    public javax.swing.JLabel dateVar;
    private javax.swing.JLabel endDateTxt;
    private javax.swing.JButton finishButton;
    private javax.swing.JLabel miscCostTxt;
    public javax.swing.JLabel miscCostVar;
    private javax.swing.JSeparator separator;
    private javax.swing.JButton setCostButton;
    private javax.swing.JButton setDateButton;
    private javax.swing.JLabel staffCostTxt;
    public javax.swing.JLabel staffCostVar;
    private javax.swing.JTabbedPane tabbs;
    private javax.swing.JLabel totalCostTxt;
    public javax.swing.JLabel totalCostVar;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.png")));
    }

    private void changeTexts() {
        pm.getConnection(id);
        pm.getData();
        staffCostVar.setText(valueCheck(pm.staffCost));
        miscCostVar.setText(valueCheck(pm.miscCost));
        if (pm.totalCost == pm.staffCost || pm.totalCost == pm.miscCost) {
            pm.totalCost = 0;
        }
        totalCostVar.setText(valueCheck(pm.totalCost));

        dateVar.setText(f.format(pm.finalDate));

        pm.closeConnection();
    }

    public String valueCheck(int value) {
        if (value == 0) {
            return "Not set";
        }
        return Integer.toString(value);
    }
}
