
package busyPoint;

import java.awt.Toolkit;


public class Misc extends javax.swing.JFrame {
    public int id = ProjectManager.id;
    public ProjectManager pm = new ProjectManager();
    public int studio, copyR;
    
    public Misc() {
        initComponents();
        setIcon();
    }

    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        studioPanel = new javax.swing.JPanel();
        studioTxt1 = new javax.swing.JLabel();
        studioVar1 = new javax.swing.JLabel();
        dayTxt = new javax.swing.JLabel();
        dayPTxt = new javax.swing.JLabel();
        studioSetPanel = new javax.swing.JPanel();
        studioTxt3 = new javax.swing.JLabel();
        studioVar2 = new javax.swing.JLabel();
        studioSlider = new javax.swing.JSlider();
        studioSliderUpButton = new javax.swing.JButton();
        studioSliderDownButton = new javax.swing.JButton();
        copyRPanel = new javax.swing.JPanel();
        copyRTxt1 = new javax.swing.JLabel();
        copyRVar1 = new javax.swing.JLabel();
        itemTxt = new javax.swing.JLabel();
        itemPTxt = new javax.swing.JLabel();
        copyRSetPanel = new javax.swing.JPanel();
        copyRTxt2 = new javax.swing.JLabel();
        copyRVar2 = new javax.swing.JLabel();
        copyRSlider = new javax.swing.JSlider();
        copyRSliderUpButton = new javax.swing.JButton();
        copyRSliderDownButton = new javax.swing.JButton();
        finishButton = new javax.swing.JButton();
        setButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Misc cost");
        setResizable(false);

        studioPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        studioTxt1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        studioTxt1.setText("Studio time:");

        studioVar1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        studioVar1.setForeground(new java.awt.Color(255, 0, 0));
        studioVar1.setText("Not set");

        dayTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        dayTxt.setText("Day price:");

        dayPTxt.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        dayPTxt.setText("£ 500");

        javax.swing.GroupLayout studioPanelLayout = new javax.swing.GroupLayout(studioPanel);
        studioPanel.setLayout(studioPanelLayout);
        studioPanelLayout.setHorizontalGroup(
            studioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studioPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(studioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(studioTxt1)
                    .addComponent(dayTxt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(studioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(dayPTxt)
                    .addComponent(studioVar1))
                .addContainerGap(93, Short.MAX_VALUE))
        );
        studioPanelLayout.setVerticalGroup(
            studioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studioPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(studioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(studioTxt1)
                    .addComponent(studioVar1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(studioPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(dayTxt)
                    .addComponent(dayPTxt))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        studioSetPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        studioTxt3.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        studioTxt3.setText("Enter the number of studio days: ");

        studioVar2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        studioVar2.setText("1");

        studioSlider.setMaximum(50);
        studioSlider.setMinimum(1);
        studioSlider.setValue(1);
        studioSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                studioSliderMoved(evt);
            }
        });

        studioSliderUpButton.setText(">");
        studioSliderUpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studioSliderUpButtonActionPerformed(evt);
            }
        });

        studioSliderDownButton.setText("<");
        studioSliderDownButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                studioSliderDownButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout studioSetPanelLayout = new javax.swing.GroupLayout(studioSetPanel);
        studioSetPanel.setLayout(studioSetPanelLayout);
        studioSetPanelLayout.setHorizontalGroup(
            studioSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studioSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(studioSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addGroup(studioSetPanelLayout.createSequentialGroup()
                        .addComponent(studioSliderDownButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(studioSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(studioSliderUpButton))
                    .addComponent(studioTxt3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(studioSetPanelLayout.createSequentialGroup()
                .addGap(111, 111, 111)
                .addComponent(studioVar2)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        studioSetPanelLayout.setVerticalGroup(
            studioSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(studioSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(studioTxt3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(studioVar2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(studioSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(studioSliderUpButton)
                    .addComponent(studioSliderDownButton)
                    .addComponent(studioSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        copyRPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        copyRTxt1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        copyRTxt1.setText("Copyright items:");

        copyRVar1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        copyRVar1.setForeground(new java.awt.Color(255, 0, 0));
        copyRVar1.setText("Not set");

        itemTxt.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        itemTxt.setText("Each item cost:");

        itemPTxt.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        itemPTxt.setText("£ 500");

        javax.swing.GroupLayout copyRPanelLayout = new javax.swing.GroupLayout(copyRPanel);
        copyRPanel.setLayout(copyRPanelLayout);
        copyRPanelLayout.setHorizontalGroup(
            copyRPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(copyRPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(copyRPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(copyRTxt1)
                    .addComponent(itemTxt))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(copyRPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(itemPTxt)
                    .addComponent(copyRVar1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        copyRPanelLayout.setVerticalGroup(
            copyRPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(copyRPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(copyRPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(copyRTxt1)
                    .addComponent(copyRVar1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(copyRPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(itemTxt)
                    .addComponent(itemPTxt))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        copyRSetPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        copyRTxt2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        copyRTxt2.setText("Enter the number of items: ");

        copyRVar2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        copyRVar2.setText("1");

        copyRSlider.setMaximum(50);
        copyRSlider.setMinimum(1);
        copyRSlider.setValue(1);
        copyRSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                copyRSliderMoved(evt);
            }
        });

        copyRSliderUpButton.setText(">");
        copyRSliderUpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyRSliderUpButtonActionPerformed(evt);
            }
        });

        copyRSliderDownButton.setText("<");
        copyRSliderDownButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyRSliderDownButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout copyRSetPanelLayout = new javax.swing.GroupLayout(copyRSetPanel);
        copyRSetPanel.setLayout(copyRSetPanelLayout);
        copyRSetPanelLayout.setHorizontalGroup(
            copyRSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(copyRSetPanelLayout.createSequentialGroup()
                .addGap(108, 108, 108)
                .addComponent(copyRVar2)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(copyRSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(copyRSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(copyRTxt2, javax.swing.GroupLayout.DEFAULT_SIZE, 208, Short.MAX_VALUE)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, copyRSetPanelLayout.createSequentialGroup()
                        .addComponent(copyRSliderDownButton)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(copyRSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(copyRSliderUpButton)))
                .addContainerGap())
        );
        copyRSetPanelLayout.setVerticalGroup(
            copyRSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(copyRSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(copyRTxt2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(copyRVar2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(copyRSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(copyRSliderUpButton)
                    .addComponent(copyRSliderDownButton)
                    .addComponent(copyRSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        finishButton.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        finishButton.setText("Finished");
        finishButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finishButtonActionPerformed(evt);
            }
        });

        setButton.setText("Set");
        setButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setCost(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(studioPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(copyRPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(studioSetPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(copyRSetPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(setButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(finishButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(studioPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(copyRPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(studioSetPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(copyRSetPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(finishButton)
                    .addComponent(setButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void studioSliderMoved(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_studioSliderMoved
        studioVar2.setText(Integer.toString(studioSlider.getValue()));
    }//GEN-LAST:event_studioSliderMoved

    private void studioSliderUpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studioSliderUpButtonActionPerformed
        studioSlider.setValue(studioSlider.getValue()+1);
        studioVar2.setText(Integer.toString(studioSlider.getValue()));
    }//GEN-LAST:event_studioSliderUpButtonActionPerformed

    private void studioSliderDownButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_studioSliderDownButtonActionPerformed
        studioSlider.setValue(studioSlider.getValue()-1);
        studioVar2.setText(Integer.toString(studioSlider.getValue()));
    }//GEN-LAST:event_studioSliderDownButtonActionPerformed

    private void copyRSliderMoved(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_copyRSliderMoved
        copyRVar2.setText(Integer.toString(copyRSlider.getValue()));
    }//GEN-LAST:event_copyRSliderMoved

    private void copyRSliderUpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyRSliderUpButtonActionPerformed
        copyRSlider.setValue(copyRSlider.getValue()+1);
        copyRVar2.setText(Integer.toString(copyRSlider.getValue()));
    }//GEN-LAST:event_copyRSliderUpButtonActionPerformed

    private void copyRSliderDownButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyRSliderDownButtonActionPerformed
        copyRSlider.setValue(copyRSlider.getValue()-1);
        copyRVar2.setText(Integer.toString(copyRSlider.getValue()));
    }//GEN-LAST:event_copyRSliderDownButtonActionPerformed

    private void finishButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finishButtonActionPerformed
        dispose();
        new Cost().setVisible(true);
    }//GEN-LAST:event_finishButtonActionPerformed

    private void setCost(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setCost
        pm.getConnection(id);
        
        studio=studioSlider.getValue();
        copyR=copyRSlider.getValue();
        
        studioVar1.setText(Integer.toString(studio));
        copyRVar1.setText(Integer.toString(copyR));
        pm.miscCost=(studio*500)+(copyR*500);
        
        
        pm.updateData();
        pm.closeConnection();
    }//GEN-LAST:event_setCost

   

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel copyRPanel;
    private javax.swing.JPanel copyRSetPanel;
    public javax.swing.JSlider copyRSlider;
    public javax.swing.JButton copyRSliderDownButton;
    public javax.swing.JButton copyRSliderUpButton;
    private javax.swing.JLabel copyRTxt1;
    private javax.swing.JLabel copyRTxt2;
    private javax.swing.JLabel copyRVar1;
    public javax.swing.JLabel copyRVar2;
    private javax.swing.JLabel dayPTxt;
    private javax.swing.JLabel dayTxt;
    private javax.swing.JButton finishButton;
    private javax.swing.JLabel itemPTxt;
    private javax.swing.JLabel itemTxt;
    private javax.swing.JButton setButton;
    private javax.swing.JPanel studioPanel;
    private javax.swing.JPanel studioSetPanel;
    public javax.swing.JSlider studioSlider;
    public javax.swing.JButton studioSliderDownButton;
    public javax.swing.JButton studioSliderUpButton;
    private javax.swing.JLabel studioTxt1;
    private javax.swing.JLabel studioTxt3;
    private javax.swing.JLabel studioVar1;
    public javax.swing.JLabel studioVar2;
    // End of variables declaration//GEN-END:variables

     private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.png")));
    } 

}
