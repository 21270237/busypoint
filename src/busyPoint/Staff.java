package busyPoint;

import java.awt.Toolkit;
import java.util.Date;
import java.util.concurrent.TimeUnit;

public class Staff extends javax.swing.JFrame {

    public int id = ProjectManager.id;
    public ProjectManager pm = new ProjectManager();
    public int advMem, admMem;

    public Staff() {
        initComponents();
        setIcon();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        advPanel = new javax.swing.JPanel();
        advTxt1 = new javax.swing.JLabel();
        advVar1 = new javax.swing.JLabel();
        advSalaryTxt1 = new javax.swing.JLabel();
        advSalaryVar = new javax.swing.JLabel();
        admPanel = new javax.swing.JPanel();
        admVar1 = new javax.swing.JLabel();
        admSalaryVar = new javax.swing.JLabel();
        admSalaryTxt1 = new javax.swing.JLabel();
        admTxt1 = new javax.swing.JLabel();
        finishButton = new javax.swing.JButton();
        advSetPanel = new javax.swing.JPanel();
        advTxt2 = new javax.swing.JLabel();
        advVar2 = new javax.swing.JLabel();
        advSlider = new javax.swing.JSlider();
        advSliderUpButton = new javax.swing.JButton();
        advSliderDownButton = new javax.swing.JButton();
        admSetPanel = new javax.swing.JPanel();
        admTxt2 = new javax.swing.JLabel();
        admVar2 = new javax.swing.JLabel();
        admSlider = new javax.swing.JSlider();
        admSliderUpButton = new javax.swing.JButton();
        admSliderDownButton = new javax.swing.JButton();
        setButton = new javax.swing.JButton();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Staff");
        setResizable(false);

        advPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        advTxt1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        advTxt1.setText("Advert members:");

        advVar1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        advVar1.setForeground(new java.awt.Color(255, 0, 0));
        advVar1.setText("Not set");

        advSalaryTxt1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        advSalaryTxt1.setText("Hourly salary: ");

        advSalaryVar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        advSalaryVar.setText("£ 17");

        javax.swing.GroupLayout advPanelLayout = new javax.swing.GroupLayout(advPanel);
        advPanel.setLayout(advPanelLayout);
        advPanelLayout.setHorizontalGroup(
            advPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(advPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(advPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(advTxt1)
                    .addComponent(advSalaryTxt1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(advPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(advSalaryVar)
                    .addComponent(advVar1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        advPanelLayout.setVerticalGroup(
            advPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(advPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(advPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(advTxt1)
                    .addComponent(advVar1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(advPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(advSalaryTxt1)
                    .addComponent(advSalaryVar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        admPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        admVar1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        admVar1.setForeground(new java.awt.Color(255, 0, 0));
        admVar1.setText("Not set");

        admSalaryVar.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        admSalaryVar.setText("£ 20");

        admSalaryTxt1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        admSalaryTxt1.setText("Hourly salary: ");

        admTxt1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        admTxt1.setText("Admin members:");

        javax.swing.GroupLayout admPanelLayout = new javax.swing.GroupLayout(admPanel);
        admPanel.setLayout(admPanelLayout);
        admPanelLayout.setHorizontalGroup(
            admPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(admPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(admPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(admTxt1)
                    .addComponent(admSalaryTxt1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(admPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(admSalaryVar)
                    .addComponent(admVar1))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        admPanelLayout.setVerticalGroup(
            admPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(admPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(admPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(admTxt1)
                    .addComponent(admVar1))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addGroup(admPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(admSalaryTxt1)
                    .addComponent(admSalaryVar))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        finishButton.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        finishButton.setText("Finished");
        finishButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                finishButtonActionPerformed(evt);
            }
        });

        advSetPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        advTxt2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        advTxt2.setText("Enter the number of advert members: ");

        advVar2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        advVar2.setText("1");

        advSlider.setMaximum(50);
        advSlider.setMinimum(1);
        advSlider.setValue(1);
        advSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                advSliderMoved(evt);
            }
        });

        advSliderUpButton.setText(">");
        advSliderUpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                advSliderUpButtonActionPerformed(evt);
            }
        });

        advSliderDownButton.setText("<");
        advSliderDownButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                advSliderDownButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout advSetPanelLayout = new javax.swing.GroupLayout(advSetPanel);
        advSetPanel.setLayout(advSetPanelLayout);
        advSetPanelLayout.setHorizontalGroup(
            advSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(advSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(advSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(advSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(advSetPanelLayout.createSequentialGroup()
                            .addComponent(advSliderDownButton)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(advSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(advSliderUpButton))
                        .addComponent(advTxt2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, advSetPanelLayout.createSequentialGroup()
                        .addComponent(advVar2)
                        .addGap(115, 115, 115)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        advSetPanelLayout.setVerticalGroup(
            advSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(advSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(advTxt2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(advVar2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(advSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(advSliderUpButton)
                    .addComponent(advSliderDownButton)
                    .addComponent(advSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(52, Short.MAX_VALUE))
        );

        admSetPanel.setBorder(new javax.swing.border.SoftBevelBorder(javax.swing.border.BevelBorder.RAISED));

        admTxt2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        admTxt2.setText("Enter the number of admin members: ");

        admVar2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        admVar2.setText("1");

        admSlider.setMaximum(10);
        admSlider.setMinimum(1);
        admSlider.setValue(1);
        admSlider.addChangeListener(new javax.swing.event.ChangeListener() {
            public void stateChanged(javax.swing.event.ChangeEvent evt) {
                admSliderMoved(evt);
            }
        });

        admSliderUpButton.setText(">");
        admSliderUpButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                admSliderUpButtonActionPerformed(evt);
            }
        });

        admSliderDownButton.setText("<");
        admSliderDownButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                admSliderDownButtonActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout admSetPanelLayout = new javax.swing.GroupLayout(admSetPanel);
        admSetPanel.setLayout(admSetPanelLayout);
        admSetPanelLayout.setHorizontalGroup(
            admSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(admSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addGroup(admSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(admSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(admSetPanelLayout.createSequentialGroup()
                            .addComponent(admSliderDownButton)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(admSlider, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(admSliderUpButton))
                        .addComponent(admTxt2))
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, admSetPanelLayout.createSequentialGroup()
                        .addComponent(admVar2)
                        .addGap(115, 115, 115)))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        admSetPanelLayout.setVerticalGroup(
            admSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(admSetPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(admTxt2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(admVar2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(admSetPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(admSliderUpButton)
                    .addComponent(admSliderDownButton)
                    .addComponent(admSlider, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        setButton.setText("Set");
        setButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                setCost(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(advPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(advSetPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(admSetPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(admPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(13, Short.MAX_VALUE))
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(setButton)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(finishButton)
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(admPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(advPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(advSetPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(admSetPanel, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(finishButton)
                    .addComponent(setButton))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void advSliderDownButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_advSliderDownButtonActionPerformed
        advSlider.setValue(advSlider.getValue() - 1);
        advVar2.setText(Integer.toString(advSlider.getValue()));
    }//GEN-LAST:event_advSliderDownButtonActionPerformed

    private void advSliderUpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_advSliderUpButtonActionPerformed
        advSlider.setValue(advSlider.getValue() + 1);
        advVar2.setText(Integer.toString(advSlider.getValue()));
    }//GEN-LAST:event_advSliderUpButtonActionPerformed

    private void admSliderUpButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_admSliderUpButtonActionPerformed
        admSlider.setValue(admSlider.getValue() + 1);
        admVar2.setText(Integer.toString(admSlider.getValue()));
    }//GEN-LAST:event_admSliderUpButtonActionPerformed

    private void admSliderDownButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_admSliderDownButtonActionPerformed
        admSlider.setValue(admSlider.getValue() - 1);
        admVar2.setText(Integer.toString(admSlider.getValue()));
    }//GEN-LAST:event_admSliderDownButtonActionPerformed

    private void advSliderMoved(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_advSliderMoved
        advVar2.setText(Integer.toString(advSlider.getValue()));
    }//GEN-LAST:event_advSliderMoved

    private void admSliderMoved(javax.swing.event.ChangeEvent evt) {//GEN-FIRST:event_admSliderMoved
        admVar2.setText(Integer.toString(admSlider.getValue()));
    }//GEN-LAST:event_admSliderMoved

    private void finishButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_finishButtonActionPerformed
        dispose();
        new Cost().setVisible(true);
    }//GEN-LAST:event_finishButtonActionPerformed

    private void setCost(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_setCost
        pm.getConnection(id);
        
        advMem = advSlider.getValue();
        admMem = admSlider.getValue();
        
        advVar1.setText(Integer.toString(advMem));
        admVar1.setText(Integer.toString(admMem));

        Date curr = new Date();
        long diff = pm.finalDate.getTime() - curr.getTime();
        int hours = (int) (8 * TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS));
        pm.staffCost = (((advMem * 17) + (admMem * 20) )* hours);

        pm.updateData();
        pm.closeConnection();

    }//GEN-LAST:event_setCost


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel admPanel;
    private javax.swing.JLabel admSalaryTxt1;
    private javax.swing.JLabel admSalaryVar;
    private javax.swing.JPanel admSetPanel;
    public javax.swing.JSlider admSlider;
    public javax.swing.JButton admSliderDownButton;
    public javax.swing.JButton admSliderUpButton;
    private javax.swing.JLabel admTxt1;
    private javax.swing.JLabel admTxt2;
    private javax.swing.JLabel admVar1;
    public javax.swing.JLabel admVar2;
    private javax.swing.JPanel advPanel;
    private javax.swing.JLabel advSalaryTxt1;
    private javax.swing.JLabel advSalaryVar;
    private javax.swing.JPanel advSetPanel;
    public javax.swing.JSlider advSlider;
    public javax.swing.JButton advSliderDownButton;
    public javax.swing.JButton advSliderUpButton;
    private javax.swing.JLabel advTxt1;
    private javax.swing.JLabel advTxt2;
    private javax.swing.JLabel advVar1;
    public javax.swing.JLabel advVar2;
    private javax.swing.JButton finishButton;
    private javax.swing.JButton setButton;
    // End of variables declaration//GEN-END:variables

    private void setIcon() {
        setIconImage(Toolkit.getDefaultToolkit().getImage(getClass().getResource("icon.png")));
    }

}
